var animations = [
    //  Entrada dos itens da primeira cena
    [
        {
            id: '#points_background', // seletor do elemento html
            duration: 300, // duração da transição da propriedade 'animations'
            animations: { // alterações que serão feitas no objeto durante a animação
                width: '97.5%',
                height: '97.5%',
                opacity:1
            }
        },
        {
            id: '#central_circle', 
            duration: 300, 
            animations: {
                width: '160px',
                height: '160px',
                left: -65,
                top: -70,
                opacity: 1
            }
        },
        {
            id: '#black',
            duration: 300,
            delayBefore: 100,
            animations: { left: 55, opacity: 1 }
        },
        {
            id: '#friday',
            duration: 300,
            delayBefore: 100,
            animations: { left: 55, opacity: 1 }
        },
        {
            id: '#caixa',
            duration: 400,
            delayBefore: 300,
            delayAfter: 3000,
            animations: {  bottom: 20, opacity:1, easing: 'ease-out' },
        },
    ],

    //  Saída dos itens da primeira cena
    [
        {
            id: '#points_background',
            duration: 100,
            animations: { width: '30%', height: '30%', opacity:0 }
        },
        {
            id: '#central_circle', 
            duration: 200, 
            animations: {
                width: '30px',
                height: '30px',
                left: 0,
                top: 0,
                opacity: 0
            }
        },
        {
            id: '#black',
            duration: 200,
            animations: { left: -200, opacity: 0 }
        },
        {
            id: '#friday',
            duration: 200,
            animations: { left: 200, opacity: 0 }
        },
        {
            id: '#caixa',
            duration: 200,
            animations: { bottom: -20, opacity: 0 },
        },
    ],

    //  Entrada dos itens da segunda cena
    [
        {
            id: '#horizonta_points_background',
            duration: 300,
            animations: { width: '98%', opacity:1 }
        },
        {
            id: '#bold_text', 
            duration: 300,
            delayBefore: 100,
            animations: {
                top: 100,
                opacity: 1
            }
        },
        {
            id: '#second_text',
            duration: 200,
            animations: { left: -200, opacity: 0 }
        },
        {
            id: '#friday',
            duration: 200,
            animations: { left: 200, opacity: 0 }
        },
        {
            id: '#caixa',
            duration: 200,
            animations: { bottom: -20, opacity: 0 },
        },
    ],
];

new YAnimation(animations, {loop: false, clearAfterEnd: false}).startAnimation();